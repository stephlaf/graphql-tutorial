# frozen_string_literal: true

Artist.destroy_all if Rails.env.development?

dave = Artist.create!(
  first_name: 'Dave',
  last_name: 'Grohl'
)

song_infos = [
  { title: 'The Pretender', description: 'Soft & Heavy', image_url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTDyJ4WI5GxPbhjptXryEIEsjF3r9ZOF1aVNg&s' },
  { title: 'Something from Nothing', description: 'Wow!', image_url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSDmi4BpWyzQxcB4zYkiCFs5DmdjjXZTvqbVw&s' },
  { title: 'Outside', description: 'Joe Walsh solo!', image_url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSDmi4BpWyzQxcB4zYkiCFs5DmdjjXZTvqbVw&s' },
  { title: 'All my Life', description: 'Djdj, djdj...!', image_url: 'https://www.musiclipse.com/wp-content/uploads/2014/03/One-By-One-Foo-Fighters.jpg' }
]

song_infos.each do |info|
  song = Song.new(
    title: info[:title],
    description: info[:description],
    image_url: info[:image_url]
  )
  song.artist = dave
  song.save!
end
